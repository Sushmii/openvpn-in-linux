#!/bin/bash

# Get user action from user input
read -p "Do you want to add or remove a user? (add/remove): " action

if [[ "$action" == "add" ]]; then
  # Get username from user input
  read -p "Enter new username: " username

  # Create a new OpenVPN user with the given username
  sudo docker run --rm -it -v openvpn-data:/etc/openvpn kylemanna/openvpn easyrsa build-client-full $username nopass

  # Create a new directory for the user's files and generate the client configuration
  sudo mkdir -p $HOME/ovpn-data/users/$username
  sudo docker run --rm -it -v openvpn-data:/etc/openvpn -v $HOME/ovpn-data/users:/tmp/clients kylemanna/openvpn ovpn_getclient $username > $HOME/ovpn-data/users/$username/$username.ovpn

  echo "User $username added successfully to the OpenVPN configuration."
elif [[ "$action" == "remove" ]]; then
  # Get username from user input
  read -p "Enter the username to remove: " username

  # Revoke the user's certificate and remove the files
  sudo docker run --rm -it -v openvpn-data:/etc/openvpn kylemanna/openvpn ovpn_revokeclient $username
  sudo rm -rf $HOME/ovpn-data/users/$username

  echo "User $username removed successfully from the OpenVPN configuration."
else
  echo "Invalid action. Please choose 'add' or 'remove'."
fi
