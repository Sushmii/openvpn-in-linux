#!/bin/bash

# Stop and remove any existing OpenVPN Docker container
docker stop openvpn
docker rm openvpn
#create dir
mkdir $HOME/ovpn-data/
# Delete existing Docker volumes
docker volume rm openvpn-data

# Create Docker volume and mount to user's home directory/ovpn-data on the host
docker volume create --driver local --opt type=none --opt o=bind --opt device=$HOME/ovpn-data openvpn-data
docker run --name openvpn -v openvpn-data:/etc/openvpn -d -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn

# Generate the CA password file
echo "openvpn" > $HOME/ovpn-data/ca-password.txt

# Store the CA password key in user's home directory/ovpn-data
docker run --rm -v openvpn-data:/etc/openvpn kylemanna/openvpn ovpn_getclient CA > $HOME/ovpn-data/ca.crt

# Create client configuration file
docker run --rm -v openvpn-data:/etc/openvpn kylemanna/openvpn ovpn_genconfig -u udp://VPN.SERVERNAME.COM

# Initialize the PKI and generate certificates and keys
docker run --rm -it -v openvpn-data:/etc/openvpn kylemanna/openvpn ovpn_initpki

# Modify the OpenVPN configuration file
echo 'push "redirect-gateway def1 bypass-dhcp"' >> $HOME/ovpn-data/openvpn.conf

# Start the OpenVPN Docker container
docker start openvpn

